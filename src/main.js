import Vue from 'vue' 
import Editor from './BpmnEditor.vue'

Vue.config.productionTip = false

new Vue({
  render: h => h(Editor),
}).$mount('#app')
