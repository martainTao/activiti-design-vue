# activiti BPMN设计器  Vue版
参考网上许多博客完成的一个基于bpmnjs的vue流程图编辑器汉化版，适应activiti的版本。

## 效果图

![image-20210127153806594](image/image-20210127153806594.png)



![image-20210127153902115](image/image-20210127153902115.png)



## 项目SetUp
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
